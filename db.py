import datetime
import os

from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    String,
    Text,
    DateTime)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


dsn = os.environ.get('CODESIGN_DSN', 'sqlite:///dev.sqlite')
engine = create_engine(dsn, echo=False)

DBSession = sessionmaker(bind=engine)

Base = declarative_base()


class AuditLog(Base):
    __tablename__ = 'audit_log'

    id = Column(Integer, primary_key=True)
    presign_id = Column(Integer, nullable=True)
    ts = Column(DateTime, default=datetime.datetime.now)
    binpkg_name = Column(Text)
    binpkg_version = Column(Text)
    binpkg_architecture = Column(Text)
    file_path = Column(Text)
    key_name = Column(Text, nullable=True)
    fhash = Column(String(64), nullable=True)
    shash = Column(String(64), nullable=True)


class PackageState(Base):
    __tablename__ = 'package_state'
    id = Column(Integer, primary_key=True)
    ts = Column(DateTime, default=datetime.datetime.now)
    template_package_name = Column(Text)
    template_package_version = Column(Text)
    state = Column(String(64), nullable=False)
    error_msg = Column(Text)
    suite_codename = Column(Text)
    architecture = Column(Text)


Base.metadata.create_all(engine)

# type: ignore
