#!/usr/bin/python3

# Copyright (C) 2017 Collabora Ltd
# 2017 Helen Koike <helen.koike@collabora.com>
#
# Ported from bash to python3 by Julien Cristau <jcristau@debian.org>
#
# Copyright (C) 2018 Dropbox, Inc.
# 2018 Luke Faraone <lfaraone@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

import argparse
import contextlib
import fcntl
import hashlib
import json
import os
import re
import subprocess
import sys
import tempfile
import requests
import logging
import sqlalchemy
import yaml
from typing import Dict

try:
    from requests_file import FileAdapter
    HAS_FILEADAPTER = True
except ImportError:
    HAS_FILEADAPTER = False

from db import AuditLog, DBSession, PackageState


class Job:
    __slots__ = 'package', 'architecture', 'version', 'tempdir', 'archive', \
                'suite_codename', 'template_unpack_dir', 'template_source_dir', \
                'files_json', 'changes_file_name', 'source'

    def __init__(self, package, architecture, suite_codename, version, archive, source):
        self.package = package
        self.architecture = architecture
        self.tempdir = ""
        self.suite_codename = suite_codename
        self.archive = archive
        self.version = version
        self.template_unpack_dir = None
        self.template_source_dir = None
        self.files_json = None
        self.changes_file_name = ''
        self.source = source

    def __str__(self):
        # return a name: value for each field in the object
        return ", ".join(["{}: {}".format(x, str(getattr(self, x))) for x in self.__slots__])

    @property
    def bin_pkgs(self):
        return self.files_json['packages']


@contextlib.contextmanager
def lock(path):
    """acquire exclusive lock on `path` using `fcntl.flock`

    Raises an exception if the lock is already held.
    """
    with open(path, 'rb') as fh:
        fcntl.flock(fh, fcntl.LOCK_EX | fcntl.LOCK_NB)
        yield
        fcntl.flock(fh, fcntl.LOCK_UN)


config = {
    'commands': {
        # path to the sign-file command from Linux
        'linux_sign_file': '/usr/lib/linux-kbuild-4.9/scripts/sign-file',
        # path to our pesign wrapper script
        'sign-efi': '/usr/local/bin/pesign-wrap'
    },
    'efi': {
        # path to the nss store
        'certdir': '/srv/codesign/pki',
        # XXX should that be per-key?
        'pin': None,
        # file containing PIN
        'pinfile': None,
    },
    'chdist': {
        'dir': os.path.expanduser(os.path.join("~", ".chdist")),
    },
    'archives': {
        # This is also used as the dput target name
        "ftp-master": {
            "deb": [
                {
                    "url": "https://deb.debian.org/debian",
                },
                {
                    "url": "https://incoming.debian.org/debian-buildd",
                    "prefix": "buildd-",
                },
            ],
            "requests": "https://incoming.debian.org/debian-buildd/project/external-signatures/requests.json",  # + .gpg
        },
        "security-master": {
            "deb": [
                {
                    "url": "https://deb.debian.org/debian-security",
                    "suffix": "/updates",
                    "nosuffixifnameendswith": "-security",
                },
                {
                    "url": "https://security-master.debian.org/debian-security-buildd",
                    "prefix": "buildd-",
                    "suffix": "/updates",
                    "nosuffixifnameendswith": "-security",
                },
            ],
            "requests": "https://security-master.debian.org/debian-security-buildd/project/external-signatures/requests.json",
            "suffix": "-security",
        },
    },
    'dput': {
        'config': None,
    },
    'maintainer': {
        'key_id': None,
    },
    'keys': {
        'requests_keyring': 'etc/external-signature-requests.kbx',
        'archive_keyring': None,
        'gnupg_home': None,
        'trusted_certs': [],
    },
    'signing-keys': {},
    'package-keys': {},
    'interactive': False,
}
logging.basicConfig()
logger = logging.getLogger('signer')
logger.setLevel(logging.DEBUG)


def hash_file(f):
    m = hashlib.sha256()
    for c in iter(lambda: f.read(4096), b''):
        m.update(c)
    return m.hexdigest()


def sign_kmod(key_config: Dict[str, str], module_path: str, signature_path: str) -> str:
    env = os.environ.copy()
    if config['efi']['pin'] is not None:
        env['KBUILD_SIGN_PIN'] = config['efi']['pin']
    # use check_output instead of check_call as sign-file seems to send random
    # stuff to stderr even when it succeeds
    subprocess.check_output(
        [
            config['commands']['linux_sign_file'], '-d', 'sha256',
            key_config['pkcs11_uri'], key_config['cert_path'], module_path
        ],
        env=env,
        stderr=subprocess.STDOUT
    )

    os.rename(module_path + '.p7s', signature_path)

    with open(signature_path, 'rb') as f:
        fhash = hash_file(f)

    return fhash


def sign_efi(key_config: Dict[str, str], efi_path: str, signature_path: str) -> str:
    env = os.environ.copy()
    if config['efi']['pin'] is not None:
        env['PESIGN_PIN'] = str(config['efi']['pin'])

    with open(signature_path, 'wb') as out:
        subprocess.check_call(
            [
                config['commands']['sign-efi'], config['efi']['certdir'],
                key_config['token'], key_config['certname'], efi_path
            ],
            env=env, stdout=out
        )

    with open(signature_path, 'rb') as f:
        return hash_file(f)


def update_or_create_chdist(suite_codename, archive):
    dist = "{}-{}".format(suite_codename, archive)

    # `chdist list` blows up if the data-dir does not exist, so short-circuit and create anyways
    chdist_is_initialised = os.path.isdir(config['chdist']['dir'])

    if not chdist_is_initialised or dist not in subprocess.check_output(["chdist", "-d", config['chdist']['dir'], "list"]).decode('ascii').strip():
        # TODO(koike): support more sections.
        # We're creating a chdist here, but we're going to nuke the sources list later, to support multiple sources
        logger.info("Creating new dist {}".format(dist))
        subprocess.check_call(["chdist", "-d", config['chdist']['dir'], "create", dist, config['archives'][archive]['deb'][0]['url'], suite_codename, "main"])

        with open(os.path.join(config['chdist']['dir'], dist, "etc", "apt", "sources.list"),
                  "w") as sourceslist:
            for target in config['archives'][archive]['deb']:
                # For security.d.o, we need to append "/updates" (suffix), but only up to buster.
                # From bullseye on the suite is instead named bullseye-security.
                use_suffix = True
                nosuffixifnameendswith = target.get("nosuffixifnameendswith")
                if nosuffixifnameendswith and suite_codename.endswith(nosuffixifnameendswith):
                    use_suffix = False
                suffix = target.get('suffix', '') if use_suffix else ''
                sourceslist.write(
                    "deb [arch={arches}] {url} {prefix}{suite_codename}{suffix} {sections}\n".format(
                        arches="amd64,arm64,armhf,i386",
                        url=target['url'],
                        prefix=target.get('prefix', ''),
                        suffix=suffix,
                        suite_codename=suite_codename,
                        sections="main",
                    )
                )
        if config['keys'].get('archive_keyring'):
            os.symlink(config['keys']['archive_keyring'], os.path.join(config['chdist']['dir'], dist, 'etc', 'apt', 'trusted.gpg.d', 'archive.asc'))

    subprocess.check_call(["chdist", "-d", config['chdist']['dir'], "apt-get", dist, "update"])


def download_pkg(job, pkg):
    logger.info("Downloading: {}".format(pkg))
    subprocess.check_call([
        "chdist", "-d", config['chdist']['dir'], "-a", job.architecture, "apt-get", "{}-{}".format(job.suite_codename, job.archive),
        "download", "{bin}={ver}".format(
            bin=pkg,
            ver=job.version,
        )
    ], cwd=job.tempdir)


def build_pkg_full_name(job, pkg):
    # omit epoch from version
    version = job.version.rsplit(":")[-1]
    return "{}_{}_{}".format(pkg, version, job.architecture)


def build_pkg_deb_name(job, pkg):
    version = job.version.replace(":", "%3a")
    return "{}_{}_{}.deb".format(pkg, version, job.architecture)


def extract_pkg(job, pkg):
    pkg_full_name = build_pkg_full_name(job, pkg)
    unpack_dir = "/".join([job.tempdir, pkg_full_name])
    os.makedirs(unpack_dir)
    subprocess.check_call([
        "dpkg", "-x", build_pkg_deb_name(job, pkg), unpack_dir
    ], cwd=job.tempdir)
    return unpack_dir


def download_template(job):
    # TODO(lfaraone): need to create per-archive suites
    update_or_create_chdist(job.suite_codename, job.archive)
    download_pkg(job, job.package)


def extract_template(job):
    job.template_unpack_dir = extract_pkg(job, job.package)
    job.template_source_dir = os.path.join(job.template_unpack_dir, "usr", "share", "code-signing", job.package, "source-template")


def get_files_json(job):
    files_json_path = "{base_dir}/usr/share/code-signing/{pkg_name}/files.json".format(base_dir=job.template_unpack_dir,
                                                                                       pkg_name=job.package)
    with open(files_json_path) as fp:
        job.files_json = json.load(fp)


# TODO: optimizations to not iterate over lists again and again
def download_and_extract_binaries(job):
    for pkg in job.bin_pkgs:
        download_pkg(job, pkg)
        extract_pkg(job, pkg)


def check_template(job):
    # Check if mandatory files exist
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'source', 'format')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'changelog')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'control')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'copyright')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'rules')))
    # Check content of debian/source/format
    with open(os.path.join(job.template_source_dir, 'debian', 'source', 'format'), 'r') as f:
        assert(f.read() == "3.0 (native)\n")
    # Check signatures folder doesn't exist
    assert(not os.path.exists(os.path.join(job.template_source_dir, 'debian', 'signatures')))
    # Check that files.json doesn't use absolute paths
    for pkg, metadata in job.bin_pkgs.items():
        assert(re.match(r'[a-z0-9][a-z0-9.+-]+\Z', pkg))
        for file in metadata['files']:
            assert(any(file["file"].startswith(prefix) for prefix in ("boot/", "lib/", "usr/")))
            assert(".." not in file["file"])
            assert(not os.path.isabs(file["file"]))
    # Check that all trusted certificates are whitelisted
    trusted_certs = config['keys']['trusted_certs']
    if '*' not in trusted_certs:
        for pkg, metadata in job.bin_pkgs.items():
            pkg_trusted_certs = metadata.get('trusted_certs')
            assert(pkg_trusted_certs is not None)
            assert(all(cert in trusted_certs for cert in pkg_trusted_certs))


def log_presign(file_path, fhash, package, version, architecture):
    s = DBSession()
    try:
        entry = AuditLog(
            binpkg_name=package,
            binpkg_version=version,
            binpkg_architecture=architecture,
            fhash=fhash,
            file_path=file_path,
        )
        s.add(entry)
        s.commit()
        return entry.id
    finally:
        s.close()


def log_signature(file_path, shash, package, version, presign_id, architecture, key_name):
    s = DBSession()
    try:
        entry = AuditLog(
            binpkg_name=package,
            binpkg_version=version,
            binpkg_architecture=architecture,
            shash=shash,
            file_path=file_path,
            presign_id=presign_id,
            key_name=key_name,
        )
        s.add(entry)
        s.commit()
    finally:
        s.close()


def sign_and_log_files(job):
    if job.source in config['package-keys']:
        key_name = config['package-keys'][job.source]
    else:
        key_name = config['package-keys']['default']
    key_config = config['signing-keys'][key_name]
    for pkg, metadata in job.bin_pkgs.items():
        maybe_interactive_print('Processing', pkg)
        for n, file in enumerate(metadata['files'], start=1):
            relative_file_path = os.path.join(build_pkg_full_name(job, pkg), file["file"])
            file_path = os.path.join(job.tempdir, build_pkg_full_name(job, pkg), file["file"])
            sig_path = os.path.join(job.template_source_dir, 'debian', 'signatures', pkg, file["file"] + ".sig")

            with open(file_path, 'rb') as f:
                fhash = hash_file(f)

            presign_id = log_presign(relative_file_path, fhash, pkg, job.version, job.architecture)

            if not os.path.exists(os.path.dirname(sig_path)):
                os.makedirs(os.path.dirname(sig_path))
            if file["sig_type"] == "efi":
                sig_hash = sign_efi(key_config, file_path, sig_path)
            elif file["sig_type"] == "linux-module":
                sig_hash = sign_kmod(key_config, file_path, sig_path)
            else:
                raise ValueError("File Type Unknown")
            log_signature(relative_file_path, sig_hash, pkg, job.version, presign_id, job.architecture, key_name)
            maybe_interactive_print('\rSigned', n, 'of', len(metadata['files']), 'files', end='')
        maybe_interactive_print()


def prepare_source(job):
    subprocess.check_call([
        "dpkg-source", "-b", "."
    ], cwd=job.template_source_dir)
    # ../linux-base_4.5_source.changes
    source_package_name = subprocess.check_output(
        ["dpkg-parsechangelog", "-S", "Source"],
        cwd=job.template_source_dir).decode('ascii').strip()
    source_package_version = subprocess.check_output(
        ["dpkg-parsechangelog", "-S", "Version"],
        cwd=job.template_source_dir).decode('ascii').strip()
    # omit epoch from version
    source_package_version_noepoch = source_package_version.rsplit(":")[-1]
    job.changes_file_name = "{}_{}_source.changes".format(source_package_name, source_package_version_noepoch)

    distribution = job.suite_codename
    distribution_suffix = config['archives'][job.archive].get('suffix')
    if distribution_suffix and not distribution.endswith(distribution_suffix):
        distribution += distribution_suffix

    subprocess.check_call([
        "dpkg-genchanges", "-S", "-DDistribution={}".format(distribution), "-UCloses",
        "-O" + os.path.join("..", job.changes_file_name),
    ], cwd=job.template_source_dir)


def sign_source(job):
    logger.debug("About to sign {} to {}".format(job.archive, job.changes_file_name))
    env = os.environ.copy()
    gnupg_home = config['keys'].get('gnupg_home')
    if gnupg_home is not None:
        env['GNUPGHOME'] = gnupg_home
    subprocess.check_call(
        ['debsign', '-S', '-k', config['maintainer']['key_id'], '--', os.path.join("..", job.changes_file_name)],
        cwd=job.template_source_dir, env=env)


def submit_source(job):
    env = os.environ.copy()
    gnupg_home = config['keys'].get('gnupg_home')
    if gnupg_home is not None:
        env['GNUPGHOME'] = gnupg_home
    cmd = ['dput']
    if config['dput'].get('config') is not None:
        cmd.extend(['--config', config['dput']['config']])
    cmd.extend(['--', job.archive, os.path.join("..", job.changes_file_name)])
    subprocess.check_call(cmd, cwd=job.template_source_dir, env=env)


def validate_config():
    # XXX: Validate configuration
    # Check that one does not specify both efi.pin and efi.pinfile
    # Check that pesign-wrapper and linux-kbuild exists
    # Check that config object has the right shape
    return


def read_packages_list(url):
    s = requests.Session()
    if HAS_FILEADAPTER:
        s.mount('file://', FileAdapter())

    requests_json = s.get(url)
    requests_gpg = s.get(url + '.gpg')

    # TODO: See if there is a better way to write this
    with tempfile.TemporaryDirectory() as tempdir:
        requests_json_f = os.path.join(tempdir, 'requests.json')
        requests_gpg_f = requests_json_f + '.gpg'
        with open(requests_json_f, "wb") as f:
            f.write(requests_json.content)
        with open(requests_gpg_f, "wb") as f:
            f.write(requests_gpg.content)
        subprocess.check_call(['gpgv', '--keyring', config['keys']['requests_keyring'], requests_gpg_f, requests_json_f])

    return requests_json.json()


def filter_packages(pkgs):
    if len(pkgs) == 0:
        return []

    s = DBSession()
    skippers = s.query(
        PackageState.template_package_name, PackageState.template_package_version,
        PackageState.architecture,
    ).filter(
        PackageState.template_package_name.in_([pkg['package'] for pkg in pkgs])
    ).having(
        sqlalchemy.or_(
            PackageState.state == 'submitted',
            sqlalchemy.and_(
                PackageState.state == 'failed',
                sqlalchemy.func.count(PackageState.state) >= 3
            )
        )
    ).group_by(
        PackageState.template_package_name, PackageState.template_package_version,
        PackageState.architecture,
        PackageState.state
    ).all()

    return [pkg for pkg in pkgs if (
        pkg['package'], pkg['version'], pkg['architecture']
    ) not in skippers]


def set_package_state(job: Job, state: str, error_msg=""):
    # TODO(lfaraone): state should be an enum
    s = DBSession()
    try:
        entry = PackageState(
            error_msg=error_msg,
            state=state,
            template_package_name=job.package,
            template_package_version=job.version,
            suite_codename=job.suite_codename,
            architecture=job.architecture
        )
        s.add(entry)
        s.commit()
    finally:
        s.close()


def get_pending(archive, url):
    packages = read_packages_list(url)['packages']
    packages = filter_packages(packages)
    for pkg in packages:
        logger.debug("pkg %s", pkg)
        yield Job(package=pkg["package"],
                  architecture=pkg["architecture"],
                  suite_codename=pkg["codename"],
                  version=pkg["version"],
                  source=pkg["source"],
                  archive=archive)


def process(job):
    with tempfile.TemporaryDirectory(prefix="codesign") as tempdir:
        try:
            job.tempdir = tempdir
            set_package_state(job, 'incomplete')
            download_template(job)
            extract_template(job)
            get_files_json(job)
            check_template(job)
            download_and_extract_binaries(job)
            sign_and_log_files(job)
            prepare_source(job)
            sign_source(job)
            set_package_state(job, 'signed')
            submit_source(job)
            set_package_state(job, 'submitted')
        except Exception as e:
            maybe_interactive_wait("Something went wrong, you can "
                                   "inspect the build to figure out what. "
                                   "The temporary directory is {}\n{}".format(tempdir, e))
            raise


def maybe_interactive_wait(prompt=""):
    if not config['interactive']:
        return
    if prompt:
        print(prompt)
        print("")
    print("Press enter to continue")
    sys.stdin.readline()


def maybe_interactive_print(*args, **kwargs):
    if not config['interactive']:
        return
    print(*args, **kwargs)


def main():
    parser = argparse.ArgumentParser(
        description='sign files for secure boot'
    )
    parser.add_argument(
        '--config', '-c', type=str, default='/etc/codesign.yaml',
        help='configuration file')
    args = parser.parse_args()
    with open(args.config) as fp:
        cp = yaml.safe_load(fp)
    config.update(cp)
    logger.debug("Configuration: {}".format(cp))
    validate_config()
    if config['efi'].get('pinfile', None) is not None:
        with open(config['efi']['pinfile'], 'r') as f:
            config['efi']['pin'] = f.read().strip()
    maybe_interactive_wait("About to start processing archives")
    success = True
    with lock(args.config):
        for archive, metadata in config['archives'].items():
            pending = get_pending(archive, metadata['requests'])
            for p in pending:
                try:
                    logger.debug("Next job: {}".format(p))
                    maybe_interactive_wait("About to start next job")
                    process(p)
                except Exception as e:
                    success = False
                    set_package_state(p, 'failed', str(e))
                    logging.exception("")
    return 0 if success else 1


if __name__ == '__main__':
    sys.exit(main())
